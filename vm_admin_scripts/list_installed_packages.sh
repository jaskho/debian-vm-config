#!/bin/bash

cat /var/lib/dpkg/status | grep '^\(Package\|Status\|Version\)' | sed 's/^Package/\nPackage/'
